import java.io.File
import java.io.FileInputStream
import java.util.Properties

plugins {
    groovy
    java
    checkstyle
    jacoco
    application
    distribution
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
    alias(libs.plugins.git.version) apply false
}

fun getProps(f: File): Properties {
    val props = Properties()
    try {
        props.load(FileInputStream(f))
    } catch (t: Throwable) {
        println("Can't read $f: $t, assuming empty")
    }
    return props
}

// we handle cases without .git directory
val props = project.file("src/main/resources/version.properties")
val dotgit = project.file(".git")
if (dotgit.exists()) {
    apply(plugin = libs.plugins.git.version.get().pluginId)
    val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
    val details = versionDetails()
    val baseVersion = details.lastTag.substring(1)
    version = when {
        details.isCleanTag -> baseVersion
        else -> baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"
    }
} else if (props.exists()) { // when version.properties already exist, just use it.
    version = getProps(props).getProperty("version")
}

tasks.register("writeVersionFile") {
    val folder = project.file("src/main/resources")
    if (!folder.exists()) {
        folder.mkdirs()
    }
    props.delete()
    props.appendText("version=" + project.version)
}

tasks.getByName("jar") {
    dependsOn("writeVersionFile")
}

group = "tokyo.northside"

application {
    mainClass.set("tokyo.northside.ebview.EBViewer")
    mainModule.set("ebviewer")
    executableDir = ""
}

application.applicationDistribution.into("") {
    from("README.md", "COPYING")
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation(libs.commons.io)
    implementation(libs.commons.lang3)
    implementation(libs.url.protocol.handler)

    // preference parser
    implementation(libs.jackson.core)
    implementation(libs.jackson.databind)

    // Credentials encryption
    implementation(libs.jasypt)

    // for epwings
    implementation(libs.eb4j)
    // for pdic
    implementation(libs.pdic4j)
    // for stardict
    implementation(libs.stardict4j)
    // for lingvodsl
    implementation(libs.dsl4j)
    // for mdict
    implementation(libs.mdict4j)
    implementation(libs.jsoup)
    // for oxford-api
    implementation(libs.oxford.dictionaries)

    // for video replay
    implementation(libs.vlcj)
    // GUI theme
    implementation(libs.flatlaf)

    // logging
    implementation(libs.slf4j.api)
    implementation(libs.slf4j.simple)

    testImplementation(libs.groovy.all)
    testImplementation(libs.junit.jupiter)
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()

    // Test in headless mode with ./gradlew test -Pheadless
    if (project.hasProperty("headless")) {
        systemProperty("java.awt.headless", "true")
    }
}

spotbugs {
    reportLevel.set(com.github.spotbugs.snom.Confidence.HIGH)
}

jacoco {
    toolVersion="0.8.6"
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
        html.required.set(true)
    }
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xlint:deprecation")
    options.compilerArgs.add("-Xlint:unchecked")
}

// Disable .tar distributions
tasks.getByName("distTar").enabled = false

distributions {
    create("source") {
        contents {
            from (".")
            exclude ("out", "build", ".gradle", ".github", ".idea", ".gitignore")
        }
    }
}

// generate native command distribution zip file for each supported platform.
tasks.register<Zip>("zipExecutable") {
    // dependsOn(tasks.nativeImage)

    // distribution contents
    from("README.md")
    from("COPYING")

    // output zip file path and name
    destinationDirectory.set(file("$buildDir/distributions"))
}
