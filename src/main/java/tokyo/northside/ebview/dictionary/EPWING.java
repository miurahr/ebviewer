package tokyo.northside.ebview.dictionary;

import tokyo.northside.ebview.data.IDictionary;
import tokyo.northside.ebview.dictionary.epwing.EBDictionary;

import java.io.File;
import java.util.Set;

/**
 * Driver for EPWING.
 * @author Hiroshi Miura
 */
public class EPWING implements IDictionaryFactory {

    /**
     * Determine whether or not the supplied file is supported by this factory.
     * This is intended to be a lightweight check, e.g. looking for a file
     * extension.
     *
     * @param file The file to check
     * @return Whether or not the file is supported
     */
    @Override
    public boolean isSupportedFile(final File file) {
        return file.getPath().toUpperCase().endsWith("CATALOGS");
    }

    /**
     * Load the given file and return an {@link IDictionary} that wraps it.
     *
     * @param file The file to load
     * @return An IDictionary file that can read articles from the file
     * @throws Exception If the file could not be loaded for reasons that were not
     *                   determined by {@link #isSupportedFile(File)}
     */
    @Override
    public Set<IDictionary> loadDict(final File file) throws Exception {
        EBDictionary ebDictionary = new EBDictionary(file);
        return ebDictionary.getEBDictionarySubBooks();
    }
}
