package tokyo.northside.ebview.gui;

import tokyo.northside.ebview.data.DictionaryEntry;

import java.util.List;

/**
 * Interface to update contents from thread.
 */
public interface IThreadPane {

    void setFoundResult(List<DictionaryEntry> data);

}
