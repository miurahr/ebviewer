package tokyo.northside.ebview.gui;

import javax.swing.JFrame;
import java.awt.Font;

public interface IMainWindow {
    void showMessage(String msg);

    JFrame getApplicationFrame();

    Font getApplicationFont();
}
