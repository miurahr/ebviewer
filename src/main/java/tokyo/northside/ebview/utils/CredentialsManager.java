/*
 * EBViewer, a dictionary viewer application.
 * Copyright (C) 2022 Hiroshi Miura.
 *               2016 Aaron Madlon-Kay
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.ebview.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tokyo.northside.ebview.gui.dialogs.PasswordEnterDialogController;
import tokyo.northside.ebview.gui.dialogs.PasswordSetDialogController;
import org.apache.commons.lang3.StringUtils;
import org.jasypt.exceptions.AlreadyInitializedException;
import org.jasypt.exceptions.EncryptionInitializationException;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;

import javax.swing.FocusManager;
import java.awt.Window;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

/**
 * A class for storing and retrieving sensitive values such as login
 * credentials, API keys, etc., from the program-wide Preferences store.
 * <p>
 * Stored values are encrypted with a "master password" (=encryption key). If
 * this has not yet been supplied to the encryption engine, the user will be
 * prompted to create it. Upon creating a master password, a "canary" value is
 * saved to preferences; the canary is used to ensure that all values are
 * encrypted with the same master password (thus ensuring that the user only
 * needs to remember one password).
 * <p>
 * The user can choose not to set a master password; in this case a master
 * password is generated for the user and stored in Preferences in plain text.
 * Values stored with the CredentialsManager will still be encrypted, but
 * because the master password is readily accessible the actual security is
 * greatly diminished. This feature was deemed required for usability, despite
 * the drawbacks.
 *
 * @author Aaron Madlon-Kay
 */
public final class CredentialsManager {

    public interface IPasswordPrompt {
        Optional<char[]> getExistingPassword(String message);
        PasswordSetResult createNewPassword();
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(CredentialsManager.class);
    private static final String CREDENTIALS_MANAGER_CANARY = "credentials_manager_canary";
    private static final String CREDENTIALS_MASTER_PASSWORD = "credentials_master_password";

    private static class SingletonHelper {
        private static final CredentialsManager INSTANCE = new CredentialsManager();
    }

    public static CredentialsManager getInstance() {
        return SingletonHelper.INSTANCE;
    }

    private final IPasswordPrompt prompt;
    private BasicTextEncryptor textEncryptor;

    private CredentialsManager() {
        prompt = new GuiPasswordPrompt();
        textEncryptor = new BasicTextEncryptor();
    }

    /**
     * Securely store a key-value pair. If the master password is not stored and
     * has not been input, the user will be prompted to input it.
     *
     * @param key
     *            The key for the value to store (not encrypted)
     * @param value
     *            The value to store (encrypted)
     * @return True if the value was stored successfully; false if otherwise
     *         (e.g. the user canceled)
     */
    public boolean store(String key, String value) {
        if (value.isEmpty()) {
            clear(key);
            return true;
        }
        Optional<String> encrypted = encrypt(value);
        encrypted.ifPresent(ev -> Preferences.setPreference(key, ev));
        return encrypted.isPresent();
    }

    /**
     * Check to see if a value has been securely stored for the given key.
     * <p>
     * If the master password has not been set, this will return false for all
     * keys.
     *
     * @see #isMasterPasswordSet()
     */
    public boolean isStored(String key) {
        return isMasterPasswordSet() && !Preferences.getPreference(key).isEmpty();
    }

    private synchronized Optional<String> encrypt(String text) {
        while (true) {
            try {
                return Optional.of(textEncryptor.encrypt(text));
            } catch (EncryptionInitializationException e) {
                if (!onEncryptionFailed()) {
                    return Optional.empty();
                }
            }
        }
    }

    private void setEncryptionKey(char[] password) {
        try {
            textEncryptor.setPasswordCharArray(password);
        } catch (AlreadyInitializedException e) {
            textEncryptor = new BasicTextEncryptor();
            setEncryptionKey(password);
        }
    }

    private void setMasterPassword(char[] masterPassword) {
        setEncryptionKey(masterPassword);
        store(CREDENTIALS_MANAGER_CANARY, CREDENTIALS_MANAGER_CANARY);
    }

    /**
     * Check whether or not the master password has been set. This checks only
     * for the presence of the canary value.
     */
    public boolean isMasterPasswordSet() {
        return !StringUtils.isEmpty(Preferences.getPreference(CREDENTIALS_MANAGER_CANARY));
    }

    /**
     * Check whether or not the master password is stored in plain text so the user doesn't need to input it.
     * The master password is considered to not be stored if {@link #isMasterPasswordSet()} returns false.
     */
    public boolean isMasterPasswordStored() {
        return isMasterPasswordSet() && !StringUtils.isEmpty(Preferences.getPreference(CREDENTIALS_MASTER_PASSWORD));
    }

    /**
     * Clear the stored master password (if present) and the canary value.
     * Afterwards, any encrypted values will be considered to be not set
     * ({@link #isStored(String)} returns false; {@link #retrieve(String)}
     * returns {@link Optional#empty()}).
     */
    public void clearMasterPassword() {
        clear(CREDENTIALS_MANAGER_CANARY);
        clear(CREDENTIALS_MASTER_PASSWORD);
        synchronized (this) {
            textEncryptor = new BasicTextEncryptor();
        }
    }

    /**
     * Clear the value for the given key.
     */
    public void clear(String key) {
        Preferences.setPreference(key, "");
    }

    /**
     * Retrieve the securely stored value for the given key. If the master
     * password is not stored and has not been input, the user will be prompted
     * to input it.
     *
     * @param key
     *            The key for the value to store (not encrypted)
     * @return The Optional-wrapped value, which can be empty if the user
     *         declines to enter the master password or the master password is
     *         not the correct encryption key for the value
     */
    public Optional<String> retrieve(String key) {
        String encrypted = Preferences.getPreference(key);
        if (StringUtils.isEmpty(encrypted)) {
            return Optional.empty();
        }
        return decrypt(encrypted);
    }

    private Optional<String> decrypt(String text) {
        if (!isMasterPasswordSet()) {
            LOGGER.warn("Trying to retrieve encrypted credentials but no master password has been set.");
            return Optional.empty();
        }
        synchronized (this) {
            while (true) {
                try {
                    return Optional.of(textEncryptor.decrypt(text));
                } catch (EncryptionOperationNotPossibleException e) {
                    LOGGER.error("Could not decrypt stored credential with supposedly correct master password.");
                    return Optional.empty();
                } catch (EncryptionInitializationException e) {
                    if (!onDecryptionFailed()) {
                        return Optional.empty();
                    }
                }
            }
        }
    }

    private boolean onEncryptionFailed() {
        if (isMasterPasswordSet()) {
            if (useStoredMasterPassword()) {
                return true;
            }
            return promptForExistingPassword();
        } else {
            return promptForCreatingPassword();
        }
    }

    private boolean useStoredMasterPassword() {
        String mp = Preferences.getPreference(CREDENTIALS_MASTER_PASSWORD);
        if (!mp.isEmpty()) {
            setEncryptionKey(mp.toCharArray());
            if (checkCanary()) {
                return true;
            }
        }
        return false;
    }

    private boolean promptForCreatingPassword() {
        PasswordSetResult result = prompt.createNewPassword();
        switch (result.responseType) {
        case USE_INPUT:
            setMasterPassword(result.password);
            Arrays.fill(result.password, '\0');
            return true;
        case GENERATE_AND_STORE:
            String pwd = UUID.randomUUID().toString();
            setMasterPassword(pwd.toCharArray());
            Preferences.setPreference(CREDENTIALS_MASTER_PASSWORD, pwd);
            return true;
        case CANCEL:
            return false;
        }
        throw new IllegalArgumentException("Unknown response: " + result.responseType);
    }

    private boolean onDecryptionFailed() {
        if (!isMasterPasswordSet()) {
            return false;
        }
        if (useStoredMasterPassword()) {
            return true;
        }
        return promptForExistingPassword();
    }

    private boolean promptForExistingPassword() {
        String message = LStrings.getString("PASSWORD_ENTER_MESSAGE");
        while (true) {
            Optional<char[]> result = prompt.getExistingPassword(message);
            if (result.isPresent()) {
                setEncryptionKey(result.get());
                if (checkCanary()) {
                    return true;
                } else {
                    message = LStrings.getString("PASSWORD_TRY_AGAIN_MESSAGE");
                }
            } else {
                LOGGER.info("User declined to input master password");
                return false;
            }
        }
    }

    private boolean checkCanary() {
        if (!isMasterPasswordSet()) {
            return false;
        }
        try {
            String decrypted = textEncryptor.decrypt(Preferences.getPreference(CREDENTIALS_MANAGER_CANARY));
            return CREDENTIALS_MANAGER_CANARY.equals(decrypted);
        } catch (Exception e) {
            return false;
        }
    }

    public enum ResponseType {
        USE_INPUT, GENERATE_AND_STORE, CANCEL
    }

    public static class PasswordSetResult {
        public final ResponseType responseType;
        public final char[] password;

        public PasswordSetResult(ResponseType responseType, char[] password) {
            this.responseType = responseType;
            this.password = password;
        }
    }

    private static class GuiPasswordPrompt implements IPasswordPrompt {

        @Override
        public Optional<char[]> getExistingPassword(String message) {
            return StaticUIUtils.returnResultFromSwingThread(() -> {
                PasswordEnterDialogController dialog = new PasswordEnterDialogController();
                dialog.show(getParentWindow(), message);
                return dialog.getResult();
            });
        }

        @Override
        public PasswordSetResult createNewPassword() {
            return StaticUIUtils.returnResultFromSwingThread(() -> {
                PasswordSetDialogController dialog = new PasswordSetDialogController();
                dialog.show(getParentWindow());
                return dialog.getResult();
            });
        }

        private Window getParentWindow() {
            Window window = FocusManager.getCurrentManager().getActiveWindow();
            // if (window == null) {
            //     window = ebViewer.getApplicationFrame();
            // }
            return window;
        }
    }

    /**
     * Retrieve a credential with the given ID. First checks temporary system properties, then falls back to
     * the program's persistent preferences. Store a credential with
     * {@link #setCredential(String, String, boolean)}.
     *
     * @param id
     *            ID or key of the credential to retrieve
     * @return the credential value in plain text
     */
    public static String getCredential(String id) {
        String property = System.getProperty(id);
        if (property != null) {
            return property;
        }
        return CredentialsManager.getInstance().retrieve(id).orElse("");
    }

    /**
     * Store a credential. Credentials are stored in temporary system properties and, if
     * <code>temporary</code> is <code>false</code>, in the program's persistent preferences encoded in
     * Base64. Retrieve a credential with {@link #getCredential(String)}.
     *
     * @param id
     *            ID or key of the credential to store
     * @param value
     *            value of the credential to store
     * @param temporary
     *            if <code>false</code>, encode with Base64 and store in persistent preferences as well
     */
    public static void setCredential(String id, String value, boolean temporary) {
        System.setProperty(id, value);
        CredentialsManager.getInstance().store(id, temporary ? "" : value);
    }

    /**
     * Determine whether a credential has been stored "temporarily" according to the definition in
     * {@link #setCredential(String, String, boolean)}. The result will be <code>false</code> if the
     * credential is not stored at all, or if it is stored permanently.
     *
     * @param id
     *            ID or key of credential
     * @return <code>true</code> only if the credential is stored temporarily
     * @see #setCredential(String, String, boolean)
     * @see #getCredential(String)
     */
    public static boolean isCredentialStoredTemporarily(String id) {
        return !CredentialsManager.getInstance().isStored(id) && !System.getProperty(id, "").isEmpty();
    }

}
