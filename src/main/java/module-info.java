module tokyo.northside.ebviewer.app {
    requires java.base;
    requires java.desktop;
    requires com.formdev.flatlaf;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires com.sun.jna;
    requires org.apache.commons.io;
    requires org.apache.commons.lang3;
    requires org.slf4j;
    requires tokyo.northside.eb4j;
    requires tokyo.northside.mdict;
    requires tokyo.northside.pdic4j;
    requires tokyo.northside.protocol;
    requires uk.co.caprica.vlcj;
}