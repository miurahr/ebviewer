/*
 * EBViewer, a dictionary viewer application.
 * Copyright (C) 2023 Hiroshi Miura.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.ebview.dictionary.epwing;

import tokyo.northside.ebview.utils.ImageUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class GaijiTest {

    @Test
    public void convertImageTest() throws IOException {
        byte[] data = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, (byte) 0xe0, 0x03, (byte) 0x80, 0x00,
                0x00, 0x00, 0x00, 0x03, (byte) 0xc0, 0x04, 0x20, 0x00, 0x20, 0x00,
                (byte) 0xe0, 0x03, 0x20, 0x04, 0x20, 0x04, 0x60, 0x03, (byte) 0xa0, 0x00, 0x00, 0x00, 0x00};
        String image = ImageUtils.convertMonoGraphic2Base64(data, 16, 16);
        assertEquals("iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEX///8AAABVwtN+AAAAKklEQVR4Xm"
                        + "NggIAHzMz8EBYj815GFhmmPwxMDAcYmWWZGIEMB6b/DhBZAHKDBT9OJvDqAAAAAElFTkSuQmCC", image);
    }
}
